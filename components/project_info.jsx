class ProjectInfo extends React.Component {
  render() {
    var request = new XMLHttpRequest();
    request.open("GET", "projects.json", false);
    request.send(null);
    var project_info = JSON.parse(request.responseText)[get_data["id"]];

    return (
      <React.Fragment>
        <div className="uk-section uk-section-secondary">
          <div className="uk-container">
            <h3>{project_info["name"]}</h3>

            <div className="uk-grid-match uk-child-width-1-3@m">
              <div>
                <p>{project_info["description"]}</p>
              </div>
              <div>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor.
                </p>
              </div>
              <div>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor.
                </p>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

ReactDOM.render(<ProjectInfo />, document.getElementById("projectinfo"));
