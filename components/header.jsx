class Header extends React.Component {
  render() {
    return (
      <nav className="uk-navbar-container">
        <div className="uk-navbar-left">
          <ul className="uk-navbar-nav">
            <li className="uk-active">
              <a href="/index.html">Home</a>
            </li>
            <li className="uk-active">
              <a href="/all_projects.html">Projects</a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

ReactDOM.render(<Header />, document.getElementById("root"));
