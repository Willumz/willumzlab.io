class ProjectList extends React.Component {
  render() {
    var request = new XMLHttpRequest();
    request.open("GET", "projects.json", false);
    request.send(null);
    var projects = JSON.parse(request.responseText);

    var projects_html = "";
    for (var i in projects) {
      projects_html += `<dt><a href="/project.html?id=${i}">${
        projects[i]["name"]
      }</a></dt><dd>${projects[i]["description"].substring(0, 250)}</dd><br />`;
    }

    console.log(projects_html);

    return (
      <React.Fragment>
        <div className="uk-container">
          <dl
            className="uk-description-list uk-description-list-divider"
            dangerouslySetInnerHTML={{
              __html: projects_html
            }}
          />
        </div>
      </React.Fragment>
    );
  }
}

ReactDOM.render(<ProjectList />, document.getElementById("projectlist"));
