class ProjectDownloadsShort extends React.Component {
  render() {
    var request = new XMLHttpRequest();
    request.open("GET", "projects.json", false);
    request.send(null);
    var project_info = JSON.parse(request.responseText)[get_data["id"]];

    var ls = project_info["latest_stable"];
    var ld = project_info["latest_development"];
    for (var i = 0; i < project_info["stable_releases"].length; i++) {
      if (project_info["stable_releases"][i][0] === ls) {
        var lslink = project_info["stable_releases"][i][1];
      }
    }
    for (var i = 0; i < project_info["development_releases"].length; i++) {
      if (project_info["development_releases"][i][0] === ld) {
        var ldlink = project_info["development_releases"][i][1];
      }
    }

    return (
      <React.Fragment>
        <div className="uk-section uk-section-primary">
          <div className="uk-container">
            <h3>Downloads</h3>

            <div className="uk-grid-match uk-child-width-1-3@m">
              <div>
                <h4>Latest:</h4>
                <br />
                <a href={lslink}>
                  <button
                    className="uk-button uk-button-default"
                    style={{ backgroundColor: "limegreen", width: 200 }}
                  >
                    Stable
                  </button>
                </a>
                <br />
                &nbsp;
                <br />
                <a href={ldlink}>
                  <button
                    className="uk-button uk-button-danger"
                    style={{ width: 200 }}
                  >
                    Development
                  </button>
                </a>
                <br />
                &nbsp;
                <br />
                <a href={"/all_releases.html?id=" + get_data["id"]}>
                  <button
                    className="uk-button uk-button-default"
                    style={{ width: 200 }}
                  >
                    All Versions
                  </button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

ReactDOM.render(
  <ProjectDownloadsShort />,
  document.getElementById("projectdownloads")
);
