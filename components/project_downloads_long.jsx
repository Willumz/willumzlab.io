class ProjectDownloadsLong extends React.Component {
  render() {
    var request = new XMLHttpRequest();
    request.open("GET", "projects.json", false);
    request.send(null);
    var project_info = JSON.parse(request.responseText)[get_data["id"]];

    var all_releases = project_info["stable_releases"].concat(
      project_info["development_releases"]
    );
    var releases_html = "";
    for (var i = 0; i < all_releases.length; i++) {
      releases_html += `<dt>
            <a href="${all_releases[i][1]}">${all_releases[i][0]}</a>
          </dt>
          <dd>${all_releases[i][2]}</dd><br />`;
    }

    var release_node = React.createElement("p");
    //release_node.outerHTML = releases_html;

    return (
      <div className="uk-section uk-section-primary">
        <div className="uk-container">
          <h3>{project_info["name"]} Downloads</h3>
          <dl
            dangerouslySetInnerHTML={(function() {
              return { __html: releases_html };
            })()}
          />
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <ProjectDownloadsLong />,
  document.getElementById("projectdownloads")
);
