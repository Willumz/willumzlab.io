function get_links(stableid, developid, projname) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            parse(this.responseXML, stableid, developid, projname);
        }
    };
    xhttp.open("GET", "downloads.xml", true);
    xhttp.send();
}

function parse(xml, stableid, developid, projname) {
    var x, i;
    var stabletxt = "";
    var developtxt = "";
    x = xml.getElementsByTagName('project');
    for (i = 0; i < x.length; i++) {
        name = x[i].getAttribute('name');
        if (projname === name) {
            var stable = x[i].getElementsByTagName("stable")[0].getElementsByTagName("release");
            for (var ii = 0; ii < stable.length; ii++) {
                stabletxt += '<a href="' + stable[ii].getAttribute("link") + '">v' + stable[ii].getAttribute("version") + '</a><br>';
            }
            var develop = x[i].getElementsByTagName("development")[0].getElementsByTagName("release");
            for (var ii = 0; ii < develop.length; ii++) {
                developtxt += '<a href="' + develop[ii].getAttribute("link") + '">v' + develop[ii].getAttribute("version") + '</a><br>';
            }
        }
    }
    document.getElementById(stableid).innerHTML = stabletxt;
    document.getElementById(developid).innerHTML = developtxt;
}